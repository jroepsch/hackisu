from Board import *
from Moves import *
from minimax import *
'''
Created on Oct 19, 2018

@author: user
'''

if __name__ == '__main__':
    b = Board()
    #b.b[2][3]=Piece(1,False)
    #b.b[4][3]=Piece(1,False)
    #b.b[1][6]=0
    #b.b[3][2].Owner=1
    #b.b[3][4]=Piece(1,False)
    #b.b[4][5]=0
    #b.b[2][7]=0
    #b.b[6][7]=0
    b.b[5][4]=Piece(0,False)

    b.printBoard()
    #AllMovesPossible(b, 0)
    newBoard=copy.deepcopy(b)
    #newBoard2=copy.deepcopy(b)
    while(True):
        yourMoves=AllMovesPossible(newBoard, 1)
        if(yourMoves==[] or didSideLose(newBoard, 1)):
            print("You lost!")
            break
        text = raw_input("Your options are: %s\n"%(AllMovesPossible(newBoard, 1)))
        arr=text.split("->")
        move=Move([])
        for pos in arr:
            
            move.extend(int(pos[1]), int(pos[4]))
            #print(x)
        #move.allMoves=arr
        print(move)
        newBoard=copy.deepcopy(newBoard)
        newBoard=genBoardWholeTurn(newBoard, move)
        
        newBoard.printBoard()
        if(didSideLose(newBoard, 0)):
            print("You won!")
            break
        val1=alphabeta(newBoard, 0, 5, -1000, 1000, 0, 0, [])
        val2 = val1[1][0]
        print("CPU uses %s"%(val2))
        #newBoard1=copy.deepcopy(newBoard2)

        newBoard=genBoardWholeTurn(newBoard, val2)
        newBoard.printBoard()
        
    #def alphabeta(board, depth, maxDepth, alpha, beta, mySide, curSide):

    #print(alphabeta(b, 0, 5, -1000, 1000, 0, 0, []))
    
    pass